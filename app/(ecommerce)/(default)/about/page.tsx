"use client";
import Image from "next/image";
import Link from "next/link";

export const dynamic = "force-dynamic";
export default function About() {
  return (
    <section className="bg-white w-full">
      <div className="text-center bg-offgreen-light py-14">
        <div className="w-full max-w-screen-xl mx-auto px-5">
          <h1 className="text-2xl font-bold lg:text-4xl">About</h1>
          <p className="text-center text-black/60 mt-2">
            Who we are. Learn more about us.
          </p>
        </div>
      </div>
      <div className="w-full max-w-screen-xl mx-auto px-5">
        <div className="grid md:grid-cols-2 gap-10 md:gap-20 lg:mt-20">
          <div className="relative grid grid-cols-1">
            <div className="relative w-full">
              <Image
                alt="working"
                sizes="100vw"
                width={1000}
                height={1000}
                src="https://woodmart.b-cdn.net/wp-content/uploads/2021/03/w-about-us1-img-1-opt.jpg.webp"
                decoding="async"
                data-nimg="fill"
                className="object-cover"
                loading="lazy"
              />
            </div>
          </div>
          <div className="relative self-center lg:ml-20">
            <p className="text-green-500 font-medium mb-4">
              SEEMINGLY ELEGANT DESIGN
            </p>
            <h2 className="text-3xl font-medium mb-5">
              About our online store
            </h2>
            <i className="mt-6 text-gray-300">
              Risus suspendisse a orci penatibus a felis suscipit consectetur
              vestibulum sodales dui cum ultricies lacus interdum.
            </i>
            <p className="mt-6 text-gray-600">
              One morning, when Gregor Samsa woke from troubled dreams, he found
              himself transformed in his bed into a horrible vermin. He lay on
              his armour-like back, and if he lifted his head a little he could
              see his brown belly, slightly domed and divided by arches into
              stiff.
            </p>
            <p className="mt-6 text-gray-600">
              Dictumst per ante cras suscipit nascetur ullamcorper in nullam
              fermentum condimentum torquent iaculis reden posuere potenti
              viverra condimentum dictumst id tellus suspendisse convallis
              condimentum.
            </p>

            <div className="my-4 py-4 border-t grid grid-cols-2 gap-4">
              <div>
                <strong>
                  <em>
                    Developed by{" "}
                    <Link href={"/"} className="text-green-600">
                      Woodmart
                    </Link>{" "}
                    team @ 2024.
                  </em>
                </strong>
              </div>
              <div>
                <div className="flex justify-end items-center space-x-1">
                  <a
                    href="#"
                    data-tooltip-target="tooltip-facebook"
                    className="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100"
                  >
                    <svg
                      className="w-4 h-4"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 8 19"
                    >
                      <path
                        fillRule="evenodd"
                        d="M6.135 3H8V0H6.135a4.147 4.147 0 0 0-4.142 4.142V6H0v3h2v9.938h3V9h2.021l.592-3H5V3.591A.6.6 0 0 1 5.592 3h.543Z"
                        clipRule="evenodd"
                      />
                    </svg>
                    <span className="sr-only">Facebook</span>
                  </a>
                  <div
                    id="tooltip-facebook"
                    role="tooltip"
                    className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip"
                  >
                    Like us on Facebook
                    <div className="tooltip-arrow" data-popper-arrow></div>
                  </div>
                  <a
                    href="#"
                    data-tooltip-target="tooltip-twitter"
                    className="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100"
                  >
                    <svg
                      className="w-4 h-4"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 20 20"
                    >
                      <path
                        fill="currentColor"
                        d="M12.186 8.672 18.743.947h-2.927l-5.005 5.9-4.44-5.9H0l7.434 9.876-6.986 8.23h2.927l5.434-6.4 4.82 6.4H20L12.186 8.672Zm-2.267 2.671L8.544 9.515 3.2 2.42h2.2l4.312 5.719 1.375 1.828 5.731 7.613h-2.2l-4.699-6.237Z"
                      />
                    </svg>
                    <span className="sr-only">Twitter</span>
                  </a>
                  <div
                    id="tooltip-twitter"
                    role="tooltip"
                    className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip"
                  >
                    Follow us on Twitter
                    <div className="tooltip-arrow" data-popper-arrow></div>
                  </div>
                  <a
                    href="#"
                    data-tooltip-target="tooltip-github"
                    className="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100"
                  >
                    <svg
                      className="w-4 h-4"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10 .333A9.911 9.911 0 0 0 6.866 19.65c.5.092.678-.215.678-.477 0-.237-.01-1.017-.014-1.845-2.757.6-3.338-1.169-3.338-1.169a2.627 2.627 0 0 0-1.1-1.451c-.9-.615.07-.6.07-.6a2.084 2.084 0 0 1 1.518 1.021 2.11 2.11 0 0 0 2.884.823c.044-.503.268-.973.63-1.325-2.2-.25-4.516-1.1-4.516-4.9A3.832 3.832 0 0 1 4.7 7.068a3.56 3.56 0 0 1 .095-2.623s.832-.266 2.726 1.016a9.409 9.409 0 0 1 4.962 0c1.89-1.282 2.717-1.016 2.717-1.016.366.83.402 1.768.1 2.623a3.827 3.827 0 0 1 1.02 2.659c0 3.807-2.319 4.644-4.525 4.889a2.366 2.366 0 0 1 .673 1.834c0 1.326-.012 2.394-.012 2.72 0 .263.18.572.681.475A9.911 9.911 0 0 0 10 .333Z"
                        clipRule="evenodd"
                      />
                    </svg>
                    <span className="sr-only">Github</span>
                  </a>
                  <div
                    id="tooltip-github"
                    role="tooltip"
                    className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip"
                  >
                    Star us on GitHub
                    <div className="tooltip-arrow" data-popper-arrow></div>
                  </div>
                  <a
                    href="#"
                    data-tooltip-target="tooltip-dribbble"
                    className="inline-flex justify-center p-2 text-gray-500 rounded-lg cursor-pointer hover:text-gray-900 hover:bg-gray-100"
                  >
                    <svg
                      className="w-4 h-4"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                    >
                      <path
                        fillRule="evenodd"
                        d="M10 0a10 10 0 1 0 10 10A10.009 10.009 0 0 0 10 0Zm6.613 4.614a8.523 8.523 0 0 1 1.93 5.32 20.094 20.094 0 0 0-5.949-.274c-.059-.149-.122-.292-.184-.441a23.879 23.879 0 0 0-.566-1.239 11.41 11.41 0 0 0 4.769-3.366ZM8 1.707a8.821 8.821 0 0 1 2-.238 8.5 8.5 0 0 1 5.664 2.152 9.608 9.608 0 0 1-4.476 3.087A45.758 45.758 0 0 0 8 1.707ZM1.642 8.262a8.57 8.57 0 0 1 4.73-5.981A53.998 53.998 0 0 1 9.54 7.222a32.078 32.078 0 0 1-7.9 1.04h.002Zm2.01 7.46a8.51 8.51 0 0 1-2.2-5.707v-.262a31.64 31.64 0 0 0 8.777-1.219c.243.477.477.964.692 1.449-.114.032-.227.067-.336.1a13.569 13.569 0 0 0-6.942 5.636l.009.003ZM10 18.556a8.508 8.508 0 0 1-5.243-1.8 11.717 11.717 0 0 1 6.7-5.332.509.509 0 0 1 .055-.02 35.65 35.65 0 0 1 1.819 6.476 8.476 8.476 0 0 1-3.331.676Zm4.772-1.462A37.232 37.232 0 0 0 13.113 11a12.513 12.513 0 0 1 5.321.364 8.56 8.56 0 0 1-3.66 5.73h-.002Z"
                        clipRule="evenodd"
                      />
                    </svg>
                    <span className="sr-only">Dribbble</span>
                  </a>
                  <div
                    id="tooltip-dribbble"
                    role="tooltip"
                    className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip"
                  >
                    Follow us on Dribbble
                    <div className="tooltip-arrow" data-popper-arrow></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="w-full max-w-screen-xl mx-auto px-5">
        <div className="grid items-center gap-8 mt-32 lg:grid-cols-2">
          <div>
            <h2 className="mb-4 text-3xl font-bold">Client Reviews</h2>
            <div>
              <p className="max-w-lg leading-7 text-slate-500">
                A wonderful serenity has taken possession of my entire soul,
                like these sweet mornings of spring which I enjoy with my whole
                heart. I am alone, and feel the charm of existence in this spot,
                which was created for the bliss of souls like mine.
              </p>
              <div className="flex items-center max-w-lg gap-6 pt-6 mt-12 border-t border-gray-200 ">
                <Image
                  alt="Avatar"
                  src="https://jobhire-next.vercel.app/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Favatar.7f8f7068.png&w=64&q=75"
                  width="55"
                  height="55"
                  decoding="async"
                  data-nimg="1"
                  className="rounded-full w-14"
                  loading="lazy"
                />
                <div>
                  <p>Daniyel Martin</p>
                  <p className="text-green-500">Developer</p>
                </div>
              </div>
            </div>
          </div>
          <div className="place-self-center">
            <Image
              alt="Clients"
              width="986"
              height="928"
              decoding="async"
              data-nimg="1"
              className="w-full max-w-lg"
              src="https://jobhire-next.vercel.app/_next/image?url=%2F_next%2Fstatic%2Fmedia%2Fclients.db39bced.png&w=1080&q=75"
            />
          </div>
        </div>
      </div>
    </section>
  );
}
