import React, { ReactNode } from "react";
type Props = {
  children?: ReactNode;
};

const Layout = ({ children }: Props) => (
  <div className="flex-1 content-container h-full max-w-5xl mx-auto bg-white flex flex-col">
    <div className="grid grid-cols-[240px_1fr] xs:grid-cols-1 py-12">
      <div>
        <div>
          <div className="hidden xs:block">
            <a
              className="flex items-center gap-x-2 text-small-regular py-2"
              href="/account"
            >
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="transform rotate-90"
              >
                <path
                  d="M4 6L8 10L12 6"
                  stroke="currentColor"
                  strokeWidth="1.5"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></path>
              </svg>
              <span>Account</span>
            </a>
          </div>
          <div className="xs:block">
            <div>
              <div className="pb-4">
                <h3 className="text-base-semi">Account</h3>
              </div>
              <div className="text-base-regular">
                <ul className="flex mb-0 justify-start items-start flex-col gap-y-4">
                  <li>
                    <a
                      className="text-ui-fg-subtle hover:text-ui-fg-base"
                      href="/account"
                    >
                      Overview
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-ui-fg-subtle hover:text-ui-fg-base"
                      href="/account/profile"
                    >
                      Profile
                    </a>
                  </li>
                  <li>
                    <a
                      className="text-ui-fg-subtle hover:text-ui-fg-base"
                      href="/account/addresses"
                    >
                      Addresses
                    </a>
                  </li>
                  <li>
                    <a
                      className="hover:text-ui-fg-base text-ui-fg-base"
                      href="/account/orders"
                    >
                      Orders
                    </a>
                  </li>
                  <li className="text-grey-700">
                    <button type="button">Log out</button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex-1">{children}</div>
    </div>
  </div>
);

export default Layout;
