import { getServerSession } from "next-auth";
import { authOptions } from "../../../../lib/auth";

export const dynamic = "force-dynamic";

export default async function Account() {
  const session = await getServerSession(authOptions);
  const user = session?.user;
  
  return (
    <div className="w-full">
      <div className="mb-8 flex flex-col gap-y-4">
        <h1 className="font-bold text-2xl">Overview</h1>
      </div>
      <div className="text-xl-semi flex justify-between items-center mb-4">
        <span>Hello {user?.name}</span>
        <span className="text-small-regular text-ui-fg-base">
          Signed in as:{" "}
          <span className="font-semibold">{user?.email}</span>
        </span>
      </div>
      <div className="flex flex-col py-8 border-t border-gray-200">
        <div className="flex flex-col gap-y-4 h-full col-span-1 row-span-2 flex-1">
          <div className="flex items-start gap-x-16 mb-6">
            <div className="flex flex-col gap-y-4">
              <h3 className="text-large-semi">Profile</h3>
              <div className="flex items-end gap-x-2">
                <span className="text-3xl-semi leading-none">75%</span>
                <span className="uppercase text-base-regular text-ui-fg-subtle">
                  Completed
                </span>
              </div>
            </div>
            <div className="flex flex-col gap-y-4">
              <h3 className="text-large-semi">Addresses</h3>
              <div className="flex items-end gap-x-2">
                <span className="text-3xl-semi leading-none">0</span>
                <span className="uppercase text-base-regular text-ui-fg-subtle">
                  Saved
                </span>
              </div>
            </div>
          </div>
          <div className="flex flex-col gap-y-4">
            <div className="flex items-center gap-x-2">
              <h3 className="text-large-semi">Recent orders</h3>
            </div>
            <ul className="flex flex-col gap-y-4">
              <span>No recent orders</span>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
