"use client";
import { Button, DatePicker, notification } from "antd";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useState } from "react";
import { CheckOutlined } from "@ant-design/icons";

type Props = {
  user: any;
};

const schema = yup.object().shape({
  name: yup.string(),
  phone: yup.string(),
  address: yup.string(),
  password: yup.string(),
});

export const dynamic = "force-dynamic";

export default function ProfileForm({ user }: Props) {
  const { register, handleSubmit } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
  });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [api, contextHolder] = notification.useNotification();

  function onChange(date: any, dateString: any) {
    console.log(date, dateString);
  }

  function openNotification(type: "success" | "error") {
    const messages = {
      success: "Update successfully!",
      error: "Update failed!",
    };

    const icons = {
      success: <CheckOutlined style={{ color: "#16a34a" }} />,
      error: <CheckOutlined style={{ color: "#108ee9" }} />,
    };

    api.info({
      message: messages[type],
      placement: "top",
      icon: icons[type],
    });
  }

  const onSubmit = async (data: any) => {
    try {
      setLoading(true);
      const req = {
        password: data.password,
        name: data.name,
        phone: data.phone,
        address: data.address,
        id: user?.id,
        email: user?.email,
      };
      const res = await fetch(`/api/user`, {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(req),
      });
      setLoading(false);
      openNotification("success");
      console.log(res);
    } catch (error: any) {
      console.log(error);
      setLoading(false);
      setError(error);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="columns-2">
          <div className="mb-3 w-full">
            <div id="name">
              <label className="form-label">Name</label>
              <input
                required
                placeholder="Enter your name"
                type="text"
                className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                {...register("name", { value: user?.name })}
              />
            </div>
          </div>
          <div className="mb-3 w-full">
            <div id="birthday">
              <label className="form-label">Birthday</label>
              <DatePicker
                onChange={onChange}
                className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
              />
            </div>
          </div>
        </div>
        <div className="columns-2">
          <div className="mb-3 w-full">
            <div id="emal">
              <label className="form-label">Email</label>
              <input
                placeholder="name@company.com"
                type="email"
                className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                value={user?.email}
                name="email"
                disabled
              />
            </div>
          </div>
          <div className="mb-3 w-full">
            <div id="phone">
              <label className="form-label">Phone</label>
              <input
                placeholder="+12-345 678 910"
                type="number"
                className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                {...register("phone", { value: user?.phone })}
              />
            </div>
          </div>
        </div>
        <div className="mb-3 w-full">
          <div id="address">
            <label className="form-label">Address</label>
            <input
              placeholder="Enter your home address"
              type="text"
              className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
              {...register("address", { value: user?.address })}
            />
          </div>
        </div>
        <div className="mb-3 w-full">
          <div id="password">
            <label className="form-label">Password</label>
            <input
              placeholder="Enter your password"
              type="password"
              className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
              {...register("password")}
            />
          </div>
        </div>
        <div className="mt-3">
          <Button
            loading={loading}
            className="mt-5 tracking-wide font-semibold bg-green-600 text-gray-100 hover:text-gray-100 py-6 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none"
            type="primary"
            htmlType="submit"
          >
            Save
          </Button>
        </div>
      </form>
      {contextHolder}
    </>
  );
}
