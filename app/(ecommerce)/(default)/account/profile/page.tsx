import { authOptions } from "../../../../../lib/auth";
import { User } from "../../../../../types";
import { serverAPI } from "../../../../api/server-api";
import ProfileForm from "./form";
import { getServerSession } from "next-auth";

export const dynamic = "force-dynamic";
export default async function Profile() {
  const user = await getUser();
  return (
    <div className="w-full">
      <div className="mb-8 flex flex-col gap-y-4">
        <h1 className="font-bold text-2xl">Profile</h1>
        <p className="text-base-regular">
          View and update your profile information, including your name, email,
          and phone number. You can also update your billing address, or change
          your password.
        </p>
      </div>
      <ProfileForm
        user={user}
      ></ProfileForm>
    </div>
  );
}

async function getUser() {
  const session = await getServerSession(authOptions);
  const user = session?.user as User;
  try {
    const res: User = await serverAPI.get(`/user/${user.id}`);
    return res;
  } catch (err) {
    return null;
  }
}
