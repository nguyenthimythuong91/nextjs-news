import React, { ReactNode } from "react";
import { FooterComponent } from "../../../components/footer";
import { Header } from "../../../components/header";

type Props = {
  children?: ReactNode;
};

const Layout = ({ children }: Props) => (
  <div>
    <Header />
    <main className="flex min-h-screen flex-col items-center justify-between mb-20">
      {children}
    </main>
    <FooterComponent />
  </div>
);

export default Layout;
