export const dynamic = "force-dynamic";
import { Suspense } from "react";
import Loading from "../../../../../components/loading";
import {  } from "next/navigation";
import Products from "../products";

export default function MarketplaceCategory(context: any) {
  const  { code } = context.params;
  const params = { categoryCode: code };
  return (
    <Suspense fallback={<Loading />}>
      <Products params={params} />
    </Suspense>
  );
}
