"use client";
import { Fragment, ReactNode } from "react";
export const dynamic = "force-dynamic";

import { Menu, Transition } from "@headlessui/react";
import { DownOutlined } from "@ant-design/icons";
import { useRouter } from "next/navigation";

const sortOptions = [
  {
    name: "Name: A-Z",
    orderByDirection: "asc",
    orderBy: "name",
    current: false,
  },
  {
    name: "Name: Z-A",
    orderByDirection: "desc",
    orderBy: "name",
    current: false,
  },
  {
    name: "Price: Low to High",
    orderByDirection: "asc",
    orderBy: "price",
    current: false,
  },
  {
    name: "Price: High to Low",
    orderByDirection: "desc",
    orderBy: "price",
    current: false,
  },
];

function classNames(...classes: any) {
  return classes.filter(Boolean).join(" ");
}
type Props = {
  children?: ReactNode;
};

const Layout = ({ children }: Props) => {
  const router = useRouter();

  return (
    <div className="bg-white w-full">
      <div
        className="flex justify-center items-center"
        style={{
          backgroundImage: "url(/carousel/blog-title-bg6-opt.jpg)",
          height: "350px",
          backgroundPosition: "center",
        }}
      >
        <h1 className="text-4xl font-bold tracking-tight text-white">
          Marketplace
        </h1>
        <div className="">
          
        </div>
      </div>
      <main className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="flex items-baseline justify-between border-b border-gray-200 pb-6 pt-12">
          <div>
          <span className="text-gray-600">Home</span>/<span className="font-bold">Marketplace</span>
            </div>
          <div className="flex items-center">
            <Menu as="div" className="relative inline-block text-left">
              <div>
                <Menu.Button className="group inline-flex justify-center text-sm font-medium text-gray-700 hover:text-gray-900">
                  Sort
                  <DownOutlined
                    className="-mr-1 ml-1 h-5 w-5 flex-shrink-0 text-gray-400 group-hover:text-gray-500"
                    aria-hidden="true"
                  />
                </Menu.Button>
              </div>

              <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <Menu.Items className="absolute right-0 z-10 mt-2 w-40 origin-top-right rounded-md bg-white shadow-2xl ring-1 ring-black ring-opacity-5 focus:outline-none">
                  <div className="py-1">
                    {sortOptions.map((option) => (
                      <Menu.Item key={option.name}>
                        {({ active }: any) => (
                          <span
                            className={classNames(
                              option.current
                                ? "font-medium text-gray-900"
                                : "text-gray-500",
                              active ? "bg-gray-100" : "",
                              "block px-4 py-2 text-sm"
                            )}
                            onClick={() => {
                              router.push(
                                `?orderBy=${option.orderBy}&orderByDirection=${option.orderByDirection}`
                              );
                            }}
                          >
                            {option.name}
                          </span>
                        )}
                      </Menu.Item>
                    ))}
                  </div>
                </Menu.Items>
              </Transition>
            </Menu>
          </div>
        </div>

        <section aria-labelledby="products-heading" className="pb-24 pt-6">
          <section className="bg-white w-full container mx-auto">
            <div className="py-8 mx-auto max-w-screen-xl lg:py-16">
              {children}
            </div>
          </section>
        </section>
      </main>
    </div>
  );
};

export default Layout;
