export const dynamic = "force-dynamic";
import { Suspense } from "react";
import Products from "./products";
import Loading from "../../../../components/loading";

type Params = { orderBy: string; orderByDirection: string };

export default function Marketplace(context: any) {
  const  { orderBy,  orderByDirection} = context.searchParams;
  const params: Params = {
    orderBy: orderBy || "",
    orderByDirection: orderByDirection || "",
  };
  return (
    <Suspense fallback={<Loading />}>
      <Products params={params} />
    </Suspense>
  );
}
