import { serverAPI } from "../../../api/server-api";
import { Product } from "../../../../types";
import ProductItem from "../../../../components/product";
export const dynamic = "force-dynamic";

export default async function Products({
  params,
}: {
  params: { orderBy?: string; orderByDirection?: string, categoryCode?: string | string[] };
}) {
  const products = await getProductByParam(
    params?.orderBy,
    params?.orderByDirection,
    params.categoryCode
  );
  if (!products || products.length === 0) {
    return <p className="mt-4 text-gray-400">No data available.</p>;
  }
  return (
    <div className="grid gap-8 lg:grid-cols-5 md:grid-cols-3 grid-cols-2">
      {products?.map((product, i) => {
        return <ProductItem key={i} product={product} />;
      })}
    </div>
  );
}

async function getProductByParam(orderBy?: string, orderByDirection?: string, categoryCode?: string | string[]) {
  try {
    const res: any = await serverAPI.get(`/product/list?categoryCode=${categoryCode}&orderBy=${orderBy}&orderByDirection=${orderByDirection}`);
    return res.data as Product[];
  } catch (err) {
    console.log(err);
    return null;
  }
}
