import Link from "next/link";
import Image from "next/image";
import { Carousels } from "../../../components/carousel";
import { Category } from "../../../types";
import { serverAPI } from "../../api/server-api";
import { Suspense } from "react";
import ProdcutList from "./productList";
import Loading from "../../../components/loading";
export const dynamic = "force-dynamic";
export default async function Home() {
  const categories = await getCategories();
  return (
    <section className="bg-white w-full container mx-auto">
      <Carousels />
      <div className="columns-6 pt-8 lg:pt-16">
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2016/09/brand-witra.png"
          />
        </Link>
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2016/09/brand-Rosenthal.png"
          />
        </Link>
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2016/09/brand-PackIt.png"
          />
        </Link>
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2017/01/nichemodern.png"
          />
        </Link>
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2016/09/brand-Magisso.png"
          />
        </Link>
        <Link
          href="/"
          className="opacity-50 hover:opacity-100 grayscale hover:grayscale-0"
        >
          <Image
            alt=""
            width={100}
            height={100}
            className="mx-auto"
            src="https://woodmart.b-cdn.net/wp-content/uploads/2016/09/brand-Louis-Poulsen.png"
          />
        </Link>
      </div>
      {categories && <div className="grid grid-rows-4 grid-cols-5 grid-flow-col gap-4 pt-8 lg:pt-16">
        {categories.length > 1 && (
            <Link className="row-span-4 col-span-3 overflow-hidden relative" href={`/marketplace/${categories[0].code}`}>
              <Image
                className="h-full w-full object-cover hover:scale-[1.3] duration-500"
                src={categories[0].imageUrl}
                height={1000}
                width={1000}
                alt={categories[0].name}
              />
              <div className="bottom-0 bg-green-600 absolute p-8 pr-12 opacity-90">
                <div className="text-white text-4xl mb-5 font-light">{categories[0].name}</div>
                <span className="text-white text-lg underline decoration-dashed font-light">VIEW MORE</span>
              </div>
            </Link>
        )}

        {categories.length > 2 && (
            <Link className="col-span-2 row-span-2 overflow-hidden relative" href={`/marketplace/${categories[1].code}`}>
              <Image
                className="h-full w-full object-cover hover:scale-[1.3] duration-500"
                src={categories[1].imageUrl}
                height={1000}
                width={1000}
                alt={categories[1].name}
              />
              <div className="bottom-0 bg-green-600 absolute p-8 pr-12 opacity-90">
                <div className="text-white text-4xl mb-5 font-light">{categories[1].name}</div>
                <span className="text-white text-lg underline decoration-dashed font-light">VIEW MORE</span>
              </div>
            </Link>
        )}

        {categories.length > 3 && (
            <Link className="row-span-2 col-span-2 overflow-hidden relative" href={`/marketplace/${categories[2].code}`}>
              <Image
                className="h-full w-full object-cover hover:scale-[1.3] duration-500"
                src={categories[2].imageUrl}
                height={1000}
                width={1000}
                alt={categories[2].name}
              />
              <div className="bottom-0 bg-green-600 absolute p-8 pr-12 opacity-90">
                <div className="text-white text-4xl mb-5 font-light">{categories[2].name}</div>
                <span className="text-white text-lg underline decoration-dashed font-light">VIEW MORE</span>
              </div>
            </Link>
        )}
      </div>}
      
      <div className="mx-auto max-w-screen-xl py-8 lg:py-16">
        <h3 className="text-center text-2xl font-bold mb-2">
          Featured Products
        </h3>
        <div className="text-center color-gray-500 mb-5">
          Will your clients accept that you go about things order.
        </div>
        <div className="grid gap-8 lg:grid-cols-5 md:grid-cols-3 grid-cols-2">
          <Suspense fallback={<Loading/>}>
            <ProdcutList />
          </Suspense>
        </div>
      </div>
    </section>
  );
}

async function getCategories() {
  try {
    const res: any = await serverAPI.get("/category/list");
    return res as Category[];
  } catch (err) {
    return null;
  }
}
