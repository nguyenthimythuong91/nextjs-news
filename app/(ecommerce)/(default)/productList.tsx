import { Product } from "../../../types";
import { serverAPI } from "../../api/server-api";
import ProductItem from "../../../components/product";
export const dynamic = "force-dynamic";

export default async function ProdcutList() {
  const products = await getProduct();

  if (!products || products.length === 0) {
    return <p className="mt-4 text-gray-400">No data available.</p>;
  }

  return products?.map((product, i) => {
    return <ProductItem key={i} product={product} />;
  });
}

async function getProduct() {
  try {
    const res: any = await serverAPI.get("/product/list");
    return res.data as Product[];
  } catch (err) {
    return null;
  }
}
