"use client";
import Image from "next/image";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useForm } from "react-hook-form";

import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { LoginOutlined } from "@ant-design/icons";
import { Button } from "antd";
import Link from "next/link";
export const dynamic = "force-dynamic";

const schema = yup.object().shape({
  email: yup.string().required("Required").email("Email invalid"),
  password: yup.string().required("Required"),
});

const LoginPage = () => {
  const { register, handleSubmit } = useForm({
    mode: "onChange",
    resolver: yupResolver(schema),
  });
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  const callbackUrl = "/";

  const onSubmit = async (data: any) => {
    try {
      setLoading(true);
      const res = await signIn("credentials", {
        redirect: false,
        email: data.email,
        password: data.password,
        callbackUrl,
      });

      setLoading(false);
      console.log(res);
      if (!res?.error) {
        router.push(callbackUrl);
      } else {
        setError("invalid email or password");
      }
    } catch (error: any) {
      setLoading(false);
      setError(error);
    }
  };

  return (
    <div className="min-h-screen bg-gray-100 text-gray-900 flex justify-center">
      <div className="max-w-screen-xl m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center flex-1">
        <div className="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
          <div>
            <Image
              width={250}
              height={100}
              src="/wood-logo-dark.svg"
              className="w-32 mx-auto"
              alt="Woodmart"
            />
          </div>
          <form
            className="mt-12 flex flex-col items-center"
            onSubmit={handleSubmit(onSubmit)}
          >
            <h1 className="text-2xl xl:text-3xl font-extrabold">Login</h1>
            <div className="w-full flex-1 mt-8">
              <div className="mx-auto max-w-xs">
                {error && (
                  <p className="text-center bg-red-300 py-2 mb-6 rounded">
                    {error}
                  </p>
                )}

                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white"
                  type="email"
                  placeholder="Email"
                  {...register("email", {
                    required: "Email Address is required",
                  })}
                />
                <input
                  className="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5"
                  type="password"
                  placeholder="Password"
                  {...register("password", {
                    required: "Password is required",
                  })}
                />
                <Button
                  icon={<LoginOutlined />}
                  loading={loading}
                  className="mt-5 tracking-wide font-semibold bg-green-600 text-gray-100 hover:text-gray-100 w-full py-6 rounded-lg hover:bg-green-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none"
                  type="primary"
                  htmlType="submit"
                >
                  Login
                </Button>
                <p className="mt-4 text-xs">
                  No account yet?{" "}
                  <Link
                    href="/auth/register"
                    className="text-ct-dark-600 font-bold text-green-600"
                  >
                    CREATE AN ACCOUNT
                  </Link>
                </p>
                <p className="mt-6 text-xs text-gray-600 text-center">
                  I agree to abide by templatanas{" "}
                  <Link
                    href="/terms"
                    className="border-b border-gray-500 border-dotted"
                  >
                    Terms of Service
                  </Link>{" "}
                  and its{" "}
                  <Link
                    href="/policy"
                    className="border-b border-gray-500 border-dotted"
                  >
                    Privacy Policy
                  </Link>
                </p>
              </div>
            </div>
          </form>
        </div>
        <div className="flex-1 bg-indigo-100 text-center hidden md:flex">
          <div
            className="w-full bg-cover bg-center bg-no-repeat rounded-e-lg"
            style={{
              backgroundImage:
                "url('https://decormatters-blog-uploads.s3.amazonaws.com/8f44c0_5e0ff7df409440bbaf9cb25d4c608bad_mv2_da9805bb01.webp')",
            }}
          ></div>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
