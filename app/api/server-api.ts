import axios, { AxiosInstance, Method } from "axios";
import { GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";
import { authOptions } from "../../lib/auth";

export interface WoodmartSession {
    woodmartToken: string;
};
const baseURL = 'https://nodejs-todo-9emm.onrender.com';

let context: GetServerSidePropsContext | null = null;
export const setContext = (_context: GetServerSidePropsContext) => {
    context = _context;
};

const isServer = () => {
    return typeof window === "undefined";
};

export const axiosInterceptorInstance = axios.create({
    baseURL,
});

axiosInterceptorInstance.interceptors.request.use(
    async (config) => {
        const session = await getServerSession(authOptions);
        const user = session?.user as WoodmartSession;
        
        
        const token = isServer() ? user?.woodmartToken : null;
        
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

axiosInterceptorInstance.interceptors.response.use(
    (response) => {
        return response;
    },
    (error) => {
        if (error?.response?.status === 401) {
            if (!isServer() && typeof window !== "undefined") {
                window.location.href = "/auth";
            }
        }

        return Promise.reject(error);
    }
);

function request<T>(
    instance: AxiosInstance,
    method: Method,
    url: string,
    data: { [key: string]: any } | string,
    headers: any
): Promise<T> {
    return new Promise((resolve, reject) => {
        (() => {
            if (method === "get") {
                return instance.request({
                    url,
                    method,
                    params: data,
                    headers,
                });
            } else {
                return instance.request({
                    url,
                    method,
                    data,
                    headers,
                });
            }
        })()
            .then((res: any) => {
                resolve(res?.data);
            })
            .catch((err: any) => {
                if (err?.response?.status === 401) {
                } else {
                    reject(err);
                }
            });
    });
}


function generateRESTFulApi(instance: AxiosInstance) {
    return {
        get: function get<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request<T>(instance, "get", endpoint, data, headers);
        },
        postJSON: function postJSON<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            },
            headers = {}
        ) {
            return request<T>(instance, "post", endpoint, data, {
                ...headers,
                "Content-Type": "application/json",
            });
        },
        putJSON: function putJSON<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            },
            headers = {}
        ) {
            return request<T>(instance, "put", endpoint, data, {
                ...headers,
                "Content-Type": "application/json",
            });
        },
        del: function del<T>(
            endpoint: string,
            id?: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request(instance, "delete", `${endpoint}${id ? '/' + id : ''}`, data, {
                ...headers,
            });
        },
        delMultiple: function del<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request(instance, "delete", `${endpoint}`, data, {
                ...headers,
            });
        },
        instance,
    };
}

export const serverAPI = generateRESTFulApi(axiosInterceptorInstance);