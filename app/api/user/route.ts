import { NextResponse } from "next/server";
import { serverAPI } from "../server-api";

export async function PUT(req: Request) {
  try {
    const { id, password, phone, address, name, email } = (await req.json()) as {
      id: string;
      name: string;
      email: string;
      password: string;
      phone: string;
      address: string;
    };

    const data = {
      name,
      phone,
      address,
      email,
      ...password?.length ? { password: password } : null
    };

    const res = await serverAPI.putJSON(`/user/${id}`, data);
    return new NextResponse(
      JSON.stringify({
        status: "error",
      }),
      { status: 200 }
    );
  } catch (error: any) {
    console.log(error.message)
    return new NextResponse(
      JSON.stringify({
        status: "error",
        message: error.message,
      }),
      { status: 500 }
    );
  }
}
