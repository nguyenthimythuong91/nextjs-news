import type { Metadata } from "next";
import "./ui/globals.scss";
import { Lato } from "next/font/google";
import { NextAuthProvider } from "./providers";
import { ConfigProvider } from "antd";

export const metadata: Metadata = {
  title: "Woodmart",
  description: "",
};
const lato = Lato({
  weight: ["300", "400", "700", "900"],
  style: ["normal", "italic"],
  subsets: ["latin"],
});

type ThemeData = {
  borderRadius?: number;
  colorPrimary?: string;
  Button?: {
    colorPrimary: string;
    algorithm?: boolean;
  };
};

const data: ThemeData = {
  colorPrimary: 'rgb(22 163 74)',
  Button: {
    colorPrimary: 'rgb(22 163 74)',
  },

};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/favicon.png" sizes="any" />
      </head>
      <body className={lato.className}>
        <ConfigProvider theme={{
          token: {
            colorPrimary: data.colorPrimary,
            borderRadius: data.borderRadius,
            colorLinkActive: data.colorPrimary,
            colorLinkHover: data.colorPrimary,
          },
          components: {
            Button: {
              colorPrimary: data.Button?.colorPrimary,
              algorithm: data.Button?.algorithm,
            }
          },
        }}>
          <NextAuthProvider>{children}</NextAuthProvider>
        </ConfigProvider>
      </body>
    </html>
  );
}
