"use client";

import { Alert } from "antd";
import NavLinks from "./nav-link";

const contentStyle: React.CSSProperties = {
  height: '450px',
  objectFit: 'cover',
  objectPosition: 'center',
  display: 'block',
  width: '100%',
};

export function Header() {
  return (
    <header>
        <Alert message="Summer 25% discount on all last year's products home decor" type="success" className="bg-green-600 rounded-none text-center text-white" closable />
        <NavLinks/>
    </header>
  );
}
