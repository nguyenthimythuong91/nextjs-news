"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import Image from "next/image";
import clsx from "clsx";
import { signOut, useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import {
  CloseOutlined,
  LogoutOutlined,
  MenuOutlined,
  ShoppingCartOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Dropdown, MenuProps } from "antd";

const items: MenuProps["items"] = [
  {
    label: (
      <Link href="/account/profile" className="mr-2">
        Profile
      </Link>
    ),
    key: "1",
    icon: <UserOutlined />,
  },
  {
    label: "Logout",
    key: "2",
    icon: <LogoutOutlined />,
    onClick: () => {
      signOut();
    },
  },
];

const authItems: MenuProps["items"] = [
  {
    label: (
      <Link
        href="/auth/login"
        className="text-ct-dark-600"
      >
        Login
      </Link>
    ),
    key: "1",
  },
  {
    label: (
      <Link href="/auth/register" className="text-ct-dark-600">
        Register
      </Link>
    ),
    key: "2",
  },
];

export default function NavLinks() {
  const pathname = usePathname();
  const { data: session } = useSession();
  const user = session?.user;
  const [sidebar, setSidebar] = useState(false);

  useEffect(() => {
    setSidebar(false);
  }, [pathname]);

  return (
    <nav className="container mx-auto py-6 border-b">
      <div className="flex flex-wrap justify-between items-center mx-auto max-w-screen-xl">
        <a href="/" className="flex items-center">
          <Image
            width={200}
            height={100}
            src="/wood-logo-dark.svg"
            className="mr-3 h-8"
            alt="FlowBite Logo"
          />
        </a>
        <div className="flex gap-4 text-lg items-center lg:order-2">
        <ShoppingCartOutlined />
          {!user && (
            <Dropdown menu={{ items: authItems }}>
              <UserOutlined />
            </Dropdown>
          )}

          {user && (
            <Dropdown menu={{ items }}>
              <UserOutlined />
            </Dropdown>
          )}
          <button
            data-collapse-toggle="mobile-menu-2"
            type="button"
            className="inline-flex items-center p-2 ml-1 text-lg text-gray-500 rounded-lg lg:hidden focus:outline-none"
            aria-controls="mobile-menu-2"
            aria-expanded="false"
            onClick={() => {
              setSidebar(!sidebar);
            }}
          >
            <span className="sr-only">Open main menu</span>

            {sidebar ? <CloseOutlined /> : <MenuOutlined />}
          </button>
        </div>
        <div
          id="mobile-menu-2"
          className={`justify-between items-center w-full lg:flex lg:w-auto lg:order-1 sidebar ${
            sidebar ? "" : "hidden"
          }`}
        >
          <ul className="flex flex-col mt-4 text-xs font-bold lg:flex-row lg:space-x-8 lg:mt-0">
            <li>
              <Link
                href="/"
                className={`block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0 ${clsx(
                  {
                    "text-green-600": pathname === "/",
                  }
                )}`}
              >
                HOME
              </Link>
            </li>
            <li>
              <Link
                href="/about"
                className={`block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0 ${clsx(
                  {
                    "text-green-600": pathname === "/about",
                  }
                )}`}
              >
                ABOUT
              </Link>
            </li>
            <li>
              <Link
                href="/marketplace"
                className={`block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0 ${clsx(
                  {
                    "text-green-600": pathname === "/marketplace",
                  }
                )}`}
              >
                MARKETPLACE
              </Link>
            </li>
            <li>
              <Link
                href="/contact"
                className={`block py-2 pr-4 pl-3 border-b border-gray-100 hover:bg-gray-50 lg:hover:bg-transparent lg:border-0 lg:hover:text-primary-700 lg:p-0 ${clsx(
                  {
                    "text-green-600": pathname === "/contact",
                  }
                )}`}
              >
                CONTACT
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
