import Image from "next/image";
import Link from "next/link";
import { Product } from "../types";

export default async function ProductItem(props: { product: Product }) {
  const { product } = props;
  return (
    <Link
      key={product.name}
      href={`/product/${product.id}`}
      className="flex flex-col items-center bg-white mb-5 xs:flex-column md:max-w-xl hover:text-green-600 border border-transparent duration-500 hover:border hover:border-green-600 hover:-translate-y-2 product-item"
    >
      <Image
        className="object-cover w-full h-auto"
        src={product.imageUrls[0]}
        alt={product.name}
        width={300}
        height={300}
      />
      <div className="flex flex-col justify-between p-4 leading-normal w-full text-center">
        <h5 className="mb-2 text-sm font-bold tracking-tight">
          {product.name}
        </h5>
        <p className="mb-3 text-xs font-normal text-gray-400 line-clamp-2">
          {product.description}
        </p>
        <p className="mb-3 font-bold text-red-600">${product?.price}.00</p>
      </div>
    </Link>
  );
}
