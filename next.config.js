/** @type {import('next').NextConfig} */
const path = require('path');

const nextConfig =  {
  experimental: {
    serverMinification: true,
  },
  images: {
    remotePatterns: [{hostname: '**'}],
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
}

module.exports = nextConfig
