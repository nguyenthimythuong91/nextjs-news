export type Category = {
  id: string;
  name: string;
  description: string;
  imageUrl: string;
  code: string;
}
