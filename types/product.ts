export type Product = {
  id: string;
  name: string;
  description: string;
  imageUrls: string[];
  price: number;
}
