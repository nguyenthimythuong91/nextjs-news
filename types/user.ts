export interface User {
    token?: string;
    id: string;
    email: string;
    name: string;
    phone: string;
    address: string;
}