import { getCookie, deleteCookie } from "cookies-next";
import axios, { AxiosInstance, Method } from 'axios';

const axiosInterceptorInstance = axios.create({
    baseURL: 'https://nodejs-todo-9emm.onrender.com',
});

// Request interceptor
axiosInterceptorInstance.interceptors.request.use(
    (config) => {
        const accessToken = getCookie('woodmartToken') || '';

        if (accessToken) {
            if (config.headers) config.headers.token = accessToken;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

axiosInterceptorInstance.interceptors.response.use(
    (response) => {

        return response;
    },
    (error) => {

        return Promise.reject(error);
    }
);

function request<T>(
    instance: AxiosInstance,
    method: Method,
    url: string,
    data: { [key: string]: any } | string,
    headers: any
): Promise<T> {
    return new Promise((resolve, reject) => {
        (() => {
            if (method === "get") {
                return instance.request({
                    url,
                    method,
                    params: data,
                    headers,
                });
            } else {
                return instance.request({
                    url,
                    method,
                    data,
                    headers,
                });
            }
        })()
            .then((res: any) => {
                resolve(res?.data);
            })
            .catch((err: any) => {
                if (err?.response?.status === 401) {
                    if (!!getCookie("woodmartToken")) {
                        deleteCookie("woodmartToken");
                    }
                } else {
                    reject(err);
                }
            });
    });
}


function generateRESTFulApi(instance: AxiosInstance) {
    return {
        get: function get<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request<T>(instance, "get", endpoint, data, headers);
        },
        postJSON: function postJSON<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            },
            headers = {}
        ) {
            return request<T>(instance, "post", endpoint, data, {
                ...headers,
                "Content-Type": "application/json",
            });
        },
        putJSON: function putJSON<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            },
            headers = {}
        ) {
            return request<T>(instance, "put", endpoint, data, {
                ...headers,
                "Content-Type": "application/json",
            });
        },
        del: function del<T>(
            endpoint: string,
            id?: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request(instance, "delete", `${endpoint}${id ? '/' + id : ''}`, data, {
                ...headers,
            });
        },
        delMultiple: function del<T>(
            endpoint: string,
            data: {
                [key: string]: any;
            } = {},
            headers = {}
        ): Promise<T> {
            return request(instance, "delete", `${endpoint}`, data, {
                ...headers,
            });
        },
        instance,
    };
}
export const baseAPI = generateRESTFulApi(axiosInterceptorInstance);
